using System;
using Xunit;
using taskmanager;
using ListeTask;

namespace tp_test
{
    public class UnitTest1
    {
        [Fact]
        public void TestTaskConstructor()
        {
            DateTime deadline=new DateTime(2019,12,25);
            Taches test = new Taches(1, "alexis", "alexis", deadline, "alexis");
             Assert.Equal("alexis", test.GetNom());
            
            Assert.Equal(1, test.GetIdentifiant());
            Assert.Equal("alexis", test.GetDescriptif());
            Assert.Equal(deadline, test.GetDeadline());
            Assert.Equal("alexis", test.GetTypes());
            
        }

         [Fact]
        public void TestTaskPresentation()
        {
            DateTime deadline=new DateTime(2019,12,25);
            Taches test = new Taches(1, "alexis", "alexis", deadline, "alexis");
             Assert.Equal("La tache alexis porte le Numero 1 et dois ce finir le 25/12/2019 00:00:00", test.presentation());
        }
    }
}
