using System;
using Xunit;
using taskmanager;
using ListeTask;

namespace tp_test
{
    public class UnitTest2
    {
        [Fact]
        public void TestNbElement() // permet de teste la fonction ajouter element et la fonction nb_element
        {
            DateTime deadline=new DateTime(2019,12,25);
            Liste liste = new Liste();
            liste.ajouter_taches(1,"tache1","descriptif",deadline,"travail");
            int nb_elment=liste.nb_element_liste();
            Assert.Equal(1,nb_elment);
            
        }

         [Fact]
        public void Testidentifiant()
        {
         
             DateTime deadline=new DateTime(2019,12,25);
            Liste liste = new Liste();
            liste.ajouter_taches(1,"tache1","descriptif",deadline,"travail");
            int nb_elment=liste.identifiantzzz();
            Assert.Equal(2,nb_elment);
          
        }


           [Fact]
        public void TestSupresion()
        {
            DateTime deadline=new DateTime(2019,12,25);
            Liste liste = new Liste();
            liste.ajouter_taches(1,"tache1","descriptif",deadline,"travail");
            string chaine=liste.supression(1);
            Assert.Equal("élément supprimer",chaine);
        }

            [Fact]
        public void TestModifNom()
        {
            DateTime deadline=new DateTime(2019,12,25);
            Liste liste = new Liste();
            liste.ajouter_taches(1,"tache1","descriptif",deadline,"travail");
            string chaine=liste.modification_nom(1,"modification apporter");
            Assert.Equal("élément modifier",chaine);
        }
        

            [Fact]
        public void TestModifdescriptif()
        {
            DateTime deadline=new DateTime(2019,12,25);
            Liste liste = new Liste();
            liste.ajouter_taches(1,"tache1","descriptif",deadline,"travail");
            string chaine=liste.modification_descriptif(1,"modification apporter");
            Assert.Equal("élément modifier",chaine);
        }
        
        


            [Fact]
        public void TestModiftypes()
        {
            DateTime deadline=new DateTime(2019,12,25);
            Liste liste = new Liste();
            liste.ajouter_taches(1,"tache1","descriptif",deadline,"travail");
            string chaine=liste.modification_types(1,"modification apporter");
            Assert.Equal("élément modifier",chaine);
        }

        
        [Fact]
        public void Test_date_limite()
        {
            DateTime deadline=new DateTime(2019,01,01);
            Liste liste = new Liste();
            liste.ajouter_taches(1,"tache1","descriptif",deadline,"travail");
            string chaine=liste.date_limite();
            Assert.Equal("Vous avez une taches ou la date es finie !! penser à la supprimer",chaine);
        }

        
    }
}
